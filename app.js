const chromeLauncher = require('chrome-launcher');
const puppeteer = require("puppeteer")
const PropertiesReader = require('properties-reader');
const lighthouse = require('lighthouse');
var fs = require('fs');

const initBrowser = require('./init_browser')

const properties = PropertiesReader('resources/properties/app.properties');
const testDate = new Date(Date.now()).toString(); 

async function runAudit() {

    let destinationURL = properties.get('TEST.url.white.label')
    const host = properties.get('TEST.host');
    var chromedriver = await initBrowser.chromeStarter(host);
    const page = await chromedriver.browser.newPage();
    await page.setViewport(chromedriver.config.viewport);

    const results = await lighthouse(destinationURL, initBrowser.chromeConfig, null);
    await saveResults(results, "flight_" + testDate + ".json");

    await page.goto(destinationURL)
    await page.waitForSelector("div.spinner", { hidden: true });
    await capture(page, "flight_" + testDate + ".png", true);
    
    const metrics = await page.metrics();
    console.log(metrics);

    initBrowser.chromeFinisher(chromedriver);
}



runAudit()

function saveResults(results, screenshotName) {
    fs.writeFileSync(properties.get('TEST.audit.folder') + screenshotName, JSON.stringify(results));
    console.log(`Lighthouse scores: ${results.reportCategories.map(c => c.name + ": " + c.score).join(', ')}`);
}

async function capture(page, screenshotName, isFullCapture) {
    await page.screenshot({ path: properties.get('TEST.screen.folder') + screenshotName, fullPage: isFullCapture });
}
