const util = require('util')
const request = require('request')
const chromeLauncher = require('chrome-launcher');
const puppeteer = require("puppeteer")

const puppeteerConfig = {
    viewport: {
        width: 1920,
        height: 1080,
    },
    browserSetting: ['--start-maximized', '--enable-viewport']
};

const chromeConfig = {
    chromeFlags: ['--disable-gpu', '--test-type', '--start-maximized'],
    logLevel: 'info',
    output: 'json',
};


module.exports = {
    chromeConfig : chromeConfig,

    chromeStarter: async function chromeStarter(host) {
        chrome = await chromeLauncher.launch(chromeConfig);
        chromeConfig.port = await chrome.port;
        const chromeWebSocketEndpoint = await fetchEndPoint(host);
        browser = await puppeteer.connect({ browserWSEndpoint: chromeWebSocketEndpoint });
        var webdriver = { browser: browser, launcher: chrome, config: puppeteerConfig}
        return await webdriver;
    },

    chromeFinisher: async function chromeFinisher(driver) {
        await driver.browser.disconnect();
        await driver.launcher.kill();
    }
};

async function fetchEndPoint(host) {
    const webSocketFetcher = await util.promisify(request)(`http://${host}:${chromeConfig.port}/json/version`);
    const chromeWebSocketEndpoint = JSON.parse(webSocketFetcher.body).webSocketDebuggerUrl;
    return chromeWebSocketEndpoint;
}
